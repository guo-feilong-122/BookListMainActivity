package cn.edu.jnu.booklistmainactivity.Data;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import android.content.Context;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import cn.edu.jnu.booklistmainactivity.R;
import cn.edu.jnu.booklistmainactivity.data.DataSaver;
import cn.edu.jnu.booklistmainactivity.data.BookItem;

@RunWith(AndroidJUnit4.class)
public class DataSaverTest {
    DataSaver dataSaverBackup;
    ArrayList<BookItem> BookItemsBackup;

    @Before
    public void setUp() throws Exception {
        dataSaverBackup=new DataSaver();
        Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        BookItemsBackup=dataSaverBackup.Load(targetContext);

    }

    @After
    public void tearDown() throws Exception {
        Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        dataSaverBackup.Save(targetContext,BookItemsBackup);
    }

    @Test
    public void saveAndLoad() {
        DataSaver dataSaver=new DataSaver();
        Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        ArrayList<BookItem> BookItems=new ArrayList<>();
        BookItem BookItem=new BookItem("测试",56.7, R.drawable.book_no_name);
        BookItems.add(BookItem);
        BookItem=new BookItem("正常",12.3, R.drawable.home);
        BookItems.add(BookItem);
        dataSaver.Save(targetContext,BookItems);

        DataSaver dataLoader=new DataSaver();
        ArrayList<BookItem> BookItemsRead=dataLoader.Load(targetContext);
        Assert.assertEquals(BookItems.size(),BookItemsRead.size());
        for(int index=0;index<BookItems.size();++index)
        {
            Assert.assertEquals(BookItems.get(index).getTitle(),BookItemsRead.get(index).getTitle());
            Assert.assertEquals(BookItems.get(index).getPrice(),BookItemsRead.get(index).getPrice(),1e-2);
            Assert.assertEquals(BookItems.get(index).getResourceId(),BookItemsRead.get(index).getResourceId());
        }

    }

}