package cn.edu.jnu.booklistmainactivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class InputBookItemActivity extends AppCompatActivity {

    public static final int RESULT_CODE_SUCCESS = 666;
    private int position;
    private EditText editTextTitle;
    private EditText editTextPrice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_shop_item);

        position= this.getIntent().getIntExtra("position",0);
        String title=this.getIntent().getStringExtra("title");
        Double price=this.getIntent().getDoubleExtra("price",0);

        editTextTitle = findViewById(R.id.edittext_shop_item_title);
        editTextPrice = findViewById(R.id.edittext_shop_item_price);

        if(null!=title)
        {
            editTextTitle.setText(title);
            editTextPrice.setText(price.toString());
        }
        Button buttonOk=findViewById(R.id.button_ok);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                Bundle bundle=new Bundle();
                bundle.putString("title",editTextTitle.getText().toString());
                double price=Double.parseDouble( editTextPrice.getText().toString());
                bundle.putDouble("price",price);
                bundle.putInt("position",position);

                intent.putExtras(bundle);
                setResult(RESULT_CODE_SUCCESS,intent);
                InputBookItemActivity.this.finish();
            }
        });
    }
    @Override
    //捕获返回按钮事件方法
    //// 完全由自己控制返回键逻辑，系统不在控制，但是有个前提是不要在Activity的onKeyDown或者OnKeyUp中拦截掉返回键
    //
    //       // 拦截：就是在OnKeyDown或者OnKeyUp中自己处理了返回键（这里处理之后return true.或者return false都会导致onBackPressed不会执行）
    //       // 不拦截：在OnKeyDown和OnKeyUp中返回super对应的方法（如果两个方法都被覆写就分别都要返回super.onKeyDown,super.onKeyUp）?????????????
    public void onBackPressed(){//按返回键*************
        Intent backIntent = new Intent();
        Bundle bundle=new Bundle();//打包数据
        bundle.putString("title",editTextTitle.getText().toString());
        double price=Double.parseDouble(editTextPrice.getText().toString());//将字符串转成double 不能输入非数字的字符串，否则点击按钮回传就闪退
        bundle.putDouble("price",price);//第一个参数是key,第二个是值
        bundle.putInt("position",position);
        backIntent.putExtras(bundle);//将打包的数据放进意图
        setResult(RESULT_CODE_SUCCESS,backIntent);
        InputBookItemActivity.this.finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){//或者这种按返回键******
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)
        {
            Intent backIntent = new Intent();
            Bundle bundle=new Bundle();//打包数据
            bundle.putString("title",editTextTitle.getText().toString());
            double price=Double.parseDouble(editTextPrice.getText().toString());//将字符串转成double 不能输入非数字的字符串，否则点击按钮回传就闪退
            bundle.putDouble("price",price);//第一个参数是key,第二个是值
            bundle.putInt("position",position);
            backIntent.putExtras(bundle);//将打包的数据放进意图
            setResult(RESULT_CODE_SUCCESS,backIntent);
            InputBookItemActivity.this.finish();
        }
        return super.onKeyDown(keyCode,event);
    }

}
